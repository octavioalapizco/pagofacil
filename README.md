# README #

Esta aplicación fue creada con symfony3 y PHP7. Los pasos para instalar la aplicación es como cualquier otra aplicacion de symfony (ver los pasos mas adelante).

Puedes ver un demo de este api en: [pagofacil.norte33.com](http://pagofacil.norte33.com)

* Iniciar sesion con el usuario: cesar, password: cesar


EL Api está documentado usando NelmioApiDocBundle, que además nos provee de un 
sandbox para probar las peticiones y respuestas (buscar la pestaña Sandbox de cada endpoint).

EL Api está restringido a usuarios registrados, usando el Bundle FosUserBundle.
Para generar el token usé el Bundle de LexikJWTAuthenticationBundle que usa llaves ssl para encryptar el token.



### Instalar Dependencias ###

composer install

### Crear la base de datos ###

php bin/console doctrine:database:create

### Crear tablas ###

php bin/console doctrine:schema:update --force

### Generar información inicial ###

php bin/console pagofacil:init

### Generar llaves SSL para la generacion del token ###
    
* openssl genrsa -out var/jwt/private.pem -aes256 4096
* openssl rsa -pubout -in var/jwt/private.pem -out var/jwt/public.pem


### Crear un usuario ###

php bin/console fos:user:create


### Notas ###

Para poder ejecutar estos endpoints es necesario primero obtener un token llamando a login_check, el token obtenido se debe mandar en los encabezados (Authorization: Bearer {token})