<?php

namespace PagofacilBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Calificacion
 *
 * @ORM\Table(name="t_calificaciones")
 * @ORM\Entity(repositoryClass="PagofacilBundle\Repository\CalificacionRepository")
 */
class Calificacion
{
    /**
     * @var int
     *
     * @ORM\Column(name="id_t_calificaciones", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var float
     *
     * @ORM\Column(name="calificacion", type="decimal", precision=5, scale=2 )
     */
    private $calificacion;
	
    /**
     * @ORM\ManyToOne(targetEntity="Materia")
     * @ORM\JoinColumn(name="id_t_materias", referencedColumnName="id")
     */
    private $materia;
    
    /**
     * @ORM\ManyToOne(targetEntity="Alumno")
     * @ORM\JoinColumn(name="id_t_usuarios", referencedColumnName="id_t_usuarios")
     */
    private $alumno;
    	
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_registro", type="datetime")
     */
    private $fechaRegistro;
	
    public function getId() {
        return $this->id;
    }

    public function getCalificacion() {
        return $this->calificacion;
    }

    public function getMateria() {
        return $this->materia;
    }

    public function getAlumno() {
        return $this->alumno;
    }

    public function getFechaRegistro(): \DateTime {
        return $this->fechaRegistro;
    }

    public function setId($id) {
        $this->id = $id;
        return $this;
    }

    public function setCalificacion($calificacion) {
        $this->calificacion = $calificacion;
        return $this;
    }

    public function setMateria($materia) {
        $this->materia = $materia;
        return $this;
    }

    public function setAlumno($alumno) {
        $this->alumno = $alumno;
        return $this;
    }

    public function setFechaRegistro(\DateTime $fechaRegistro) {
        $this->fechaRegistro = $fechaRegistro;
        return $this;
    }
    
}

