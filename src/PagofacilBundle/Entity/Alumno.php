<?php

namespace PagofacilBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Alumno
 *
 * @ORM\Table(name="t_alumnos")
 * @ORM\Entity(repositoryClass="PagofacilBundle\Repository\AlumnoRepository")
 */
class Alumno
{
    /**
     * @var int
     *
     * @ORM\Column(name="id_t_usuarios", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string")
     */
    private $nombre;
    
    /**
     * @var string
     *
     * @ORM\Column(name="ap_paterno", type="string")
     */
    private $apellidoPaterno;
    
    /**
     * @var string
     *
     * @ORM\Column(name="ap_materno", type="string")
     */
    private $apellidoMaterno;
    
    /**
     * @var int
     *
     * @ORM\Column(name="activo", type="integer")
     */
    private $activo;
    
    public function getId() {
        return $this->id;
    }

    public function getNombre() {
        return $this->nombre;
    }

    public function getApellidoPaterno() {
        return $this->apellidoPaterno;
    }

    public function getApellidoMaterno() {
        return $this->apellidoMaterno;
    }

    public function getActivo() {
        return $this->activo;
    }

    public function setId($id) {
        $this->id = $id;
        return $this;
    }

    public function setNombre($nombre) {
        $this->nombre = $nombre;
        return $this;
    }

    public function setApellidoPaterno($apellidoPaterno) {
        $this->apellidoPaterno = $apellidoPaterno;
        return $this;
    }

    public function setApellidoMaterno($apellidoMaterno) {
        $this->apellidoMaterno = $apellidoMaterno;
        return $this;
    }

    public function setActivo($activo) {
        $this->activo = $activo;
        return $this;
    }


}

