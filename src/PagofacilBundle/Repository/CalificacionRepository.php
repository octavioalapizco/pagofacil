<?php

namespace PagofacilBundle\Repository;
use Doctrine\ORM\Query\ResultSetMapping;

/**
 * CalificacionRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class CalificacionRepository extends \Doctrine\ORM\EntityRepository
{
  
    public function findCalificacionesDelAlumno($alumno){
        
        $sql='select a.id_t_usuarios, a.nombre, a.ap_paterno as apellido, m.nombre as materia, c.calificacion, c.id_t_calificaciones as calificacion_id, date_format(c.fecha_registro, "%d/%m/%Y") fecha_registro '
            .'from t_calificaciones c '
            .'LEFT JOIN t_alumnos a ON c.id_t_usuarios = a.id_t_usuarios '
            .'LEFT JOIN t_materias m ON c.id_t_materias = m.id '
            .' WHERE c.id_t_usuarios=:alumno';
        
        $em = $this->getEntityManager();
        $stmt = $em->getConnection()->prepare($sql);
        $params=['alumno'=>$alumno];
        $stmt->execute($params);
        $calificaciones= $stmt->fetchAll();
        $numCalificaciones=count($calificaciones);
        $sumCal=0;
        foreach ($calificaciones as $cal){
            $sumCal = $sumCal + floatval($cal['calificacion']);
        }
        if ($numCalificaciones==0) {
            $promedio=0;
        } else {
            $promedio=$sumCal/$numCalificaciones;
        }
        return ['promedio'=>$promedio, 'calificaciones'=>$calificaciones];

    }
    
    
}
