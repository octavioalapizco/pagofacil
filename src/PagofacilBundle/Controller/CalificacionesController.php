<?php

namespace PagofacilBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

use PagofacilBundle\Entity\Calificacion;


class CalificacionesController extends Controller
{
    /**
     * 
     * @Route("/calificaciones")
     * @Method({"POST"})
     * 
     * This is the documentation description of your method, it will appear
     * on a specific pane. It will read all the text until the first
     * annotation.
     *
     *  @ApiDoc(
     *  resource=true,
     *  description="Agrega una nueva calificacion",
     *  parameters={
     *      {"name"="alumnoId", "dataType"="int", "description": "Identificador del alumno", "required":true},
     *      {"name"="materiaId", "dataType"="int", "description":"EL identificador de la materia", "required":true},
     *      {"name"="calificacion", "dataType"="decimal", "description":"La calificacion para la materia" , "required":true}
     *  }
     * )
     */
    public function createAction(Request $request)
    {
	$alumnoId=intval($request->request->get('alumnoId'));
        $materiaId=intval($request->request->get('materiaId'));
        $valorCalificacion= floatval($request->request->get('calificacion'));
        
        $em= $this->getDoctrine()->getEntityManager();
        
        $alumno = $em->find('PagofacilBundle\Entity\Alumno', $alumnoId);
        if (!$alumno) {
            throw new \Exception('El alumno no existe');
        }
        
        $materia = $em->find('PagofacilBundle\Entity\Materia', $materiaId);
        if (!$materia) {
            throw new \Exception('La materia no existe');
        }
        
        $calificacion = new Calificacion();
        $calificacion->setAlumno($alumno);
        $calificacion->setMateria($materia);
        $calificacion->setCalificacion($valorCalificacion);
        $calificacion->setFechaRegistro(new \DateTime());
        $em->persist($calificacion);
        $em->flush();
        
        $data=['msg'=>'Calificación registrada'];
        $response= new JsonResponse($data);
        return $response;
    }
	
    /**
     * @Route("/calificaciones")
     * @Method({"GET"})
     * 
     * @ApiDoc(
     *  resource=true,
     *  description="Obtener el listado de las calificaciones del alumno y su promedio",
     *  parameters={
     *      {"name"="alumnoId", "dataType"="int", "description": "Identificador del alumno", "required":true},
     *  }
     * )
     */
    public function listAction(Request $request)
    {
        $alumnoId=$request->query->get('alumnoId');
        
        $em= $this->getDoctrine()->getEntityManager();
        $calificaciones = $em->getRepository('PagofacilBundle:Calificacion')
        ->findCalificacionesDelAlumno($alumnoId);
        
        return new JsonResponse($calificaciones);
    }
	
    /**
     * @Route("/calificaciones")
     * @Method({"PUT"})
     * 
     * @ApiDoc(
     *  resource=true,
     *  description="Actualiza una calificación",
     *  parameters={
     *      {"name"="calificacionId", "dataType"="int", "description":"EL identificador de la materia", "required":true},
     *      {"name"="calificacion", "dataType"="decimal", "description":"La calificacion para la materia" , "required":true}     
     * }
     * )
     */
    public function modifyAction(Request $request)
    {
        $calificacionId=$request->get('calificacionId');
        $valorCalificacion=floatval($request->get('calificacion'));

        $em= $this->getDoctrine()->getEntityManager();
        
        $calificacion = $em->find('PagofacilBundle\Entity\Calificacion', $calificacionId);
        if (!$calificacion) {
            throw new \Exception('La calificación no existe');
        }
        $calificacion->setCalificacion($valorCalificacion);
        $em->flush();
        
        $data=['msg'=>'Calificacion actualizada'];
        $response= new JsonResponse($data);
        return $response;
    }
	
    /**
    * @Route("/calificaciones")
    * @Method({"DELETE"})
    * @ApiDoc(
    *  resource=true,
    *  description="Borra fisicamente una calificación",
    *  parameters={
    *      {"name"="calificacionId", "dataType"="int", "description":"EL identificador de la materia", "required":true}
    * }
    * )
    */
    public function deletAction(Request $request)
    {
        $calificacionId=intval($request->get('calificacionId'));
        $em= $this->getDoctrine()->getEntityManager();
        
        $calificacion = $em->find('PagofacilBundle\Entity\Calificacion', $calificacionId);
        if (!$calificacion) {
            throw new \Exception('La calificación no existe');
        }
        
        $em->remove($calificacion);
        $em->flush();
        
        $data=['msg'=>'Calificacion eliminada'];
        $response= new JsonResponse($data);
        return $response;
    }
	
	
}
