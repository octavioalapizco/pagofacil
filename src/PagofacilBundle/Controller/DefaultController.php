<?php

namespace PagofacilBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Log\DebugLoggerInterface;
use Symfony\Component\Debug\Exception\FlattenException as FlattenException;
use Symfony\Component\HttpFoundation\JsonResponse;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

class DefaultController extends Controller
{
    /**
     * El token devuelto aqu� se debe enviar en el Header de las dem�s peticiones (Authorization: Bearer {TOKEN})
     * 
     * @Route("/login_check")
    * @Method({"POST"})
    * @ApiDoc(
    *  resource=true,
    *  description="Login: devuelve un token en caso de exito",
    *  parameters={
    *      {"name"="_username", "dataType"="string", "description":"Nombre de usuario", "required":true},
    *      {"name"="_password", "dataType"="string", "description":"Contrase�a", "required":true}
    * }
    * )
    */
    public function indexAction()
    {
        //esta funci�n en realidad nunca es ejecutada, esta ruta es cachada por el firewall 
        //su raz�n de existir es solo para documentar el api (NelmioApiDocBundle)
    }
    
    //put your code here
    public function exceptionAction(Request $request, FlattenException $exception, DebugLoggerInterface $logger = null, $format = 'html'){
        $data=['msg'=>$exception->getMessage()];
        $resp=new JsonResponse($data);
        return $resp;
    }
}
