<?php

namespace PagofacilBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

use PagofacilBundle\Entity\Alumno;
use PagofacilBundle\Entity\Materia;

/**
 * Description of InitCommand
 *
 * @author Octavio
 */
class InitCommand extends ContainerAwareCommand{
    
    protected function configure()
    {
         $this
        // the name of the command (the part after "bin/console")
        ->setName('pagofacil:init')

        // the short description shown while running "php bin/console list"
        ->setDescription('Genera la configuración inicial en la base de datos')

        // the full command description shown when running the command with
        // the "--help" option
        ->setHelp('');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln([
            'PagoFacil Init',
            '============',
            '',
        ]);
        /** @var EntityManager */
        $em =  $this->getContainer()->get('doctrine')->getEntityManager();
        
        $alumno = new Alumno();
        $alumno->setActivo(1);
        $alumno->setNombre('John');
        $alumno->setApellidoPaterno('Down');
        $alumno->setApellidoMaterno('Dow');
        $em->persist($alumno);
        
        $m1=new Materia();
        $m1->setActivo(1);
        $m1->setNombre('Matematicas');
        $em->persist($m1);
        
        $m2=new Materia();
        $m2->setActivo(1);
        $m2->setNombre('programacion');
        $em->persist($m2);
        
        $m3=new Materia();
        $m3->setActivo(1);
        $m3->setNombre('ingenieria de software');
        $em->persist($m3);
        
        $em->flush();
        
        $output->writeln([
            'Informacion Generada',
            '============',
            '',
        ]);
        
    }
}
